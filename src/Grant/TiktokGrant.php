<?php

namespace Drupal\social_auth_tiktok_decouple\Grant;

use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\simple_oauth\Entities\UserEntity;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_tiktok_decouple\TiktokAuthManager;
use gimucco\TikTokLoginKit\Connector;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Tiktok grant class.
 */
class TiktokGrant extends AbstractGrant {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\social_auth_tiktok_decouple\TiktokAuthManager definition.
   *
   * @var \Drupal\social_auth_tiktok_decouple\TiktokAuthManager
   */
  protected $socialAuthTiktokManager;

  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $networkManager;

  /**
   * The Social Auth user authenticator..
   *
   * @var \Drupal\social_auth\User\UserAuthenticator
   */
  protected $userAuthenticator;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Extension\ModuleHandler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    UserRepositoryInterface $userRepository,
    RefreshTokenRepositoryInterface $refreshTokenRepository,
    TiktokAuthManager $tiktokAuthManager,
    NetworkManager $networkManager,
    UserAuthenticator $userAuthenticator,
    AccountProxyInterface $currentUser,
    ModuleHandler $moduleHandler
  ) {
    $this->setUserRepository($userRepository);
    $this->setRefreshTokenRepository($refreshTokenRepository);
    $this->socialAuthTiktokManager = $tiktokAuthManager;
    $this->networkManager = $networkManager;
    $this->userAuthenticator = $userAuthenticator;
    $this->currentUser = $currentUser;
    $this->refreshTokenTTL = new \DateInterval('P1M');
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function respondToAccessTokenRequest(
    ServerRequestInterface $request,
    ResponseTypeInterface $responseType,
    \DateInterval $accessTokenTTL
  ) {
    $client = $this->validateClient($request);
    $scopes = $this->validateScopes($this->getRequestParameter('scope', $request, $this->defaultScope));
    $user = $this->validateUser($request, $client);

    $finalizedScopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getIdentifier());

    $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $finalizedScopes);
    $this->getEmitter()
      ->emit(new RequestEvent(RequestEvent::ACCESS_TOKEN_ISSUED, $request));
    $responseType->setAccessToken($accessToken);

    $refreshToken = $this->issueRefreshToken($accessToken);

    if ($refreshToken !== NULL) {
      $this->getEmitter()
        ->emit(new RequestEvent(RequestEvent::REFRESH_TOKEN_ISSUED, $request));
      $responseType->setRefreshToken($refreshToken);
    }

    return $responseType;
  }

  /**
   * Validate user by TikTok credentials.
   *
   * @param \Psr\Http\Message\ServerRequestInterface $request
   *   Psr\Http\Message\ServerRequestInterface.
   * @param \League\OAuth2\Server\Entities\ClientEntityInterface $client
   *   League\OAuth2\Server\Entities\ClientEntityInterface.
   *
   * @return \League\OAuth2\Server\Entities\UserEntityInterface
   *   League\OAuth2\Server\Entities\UserEntityInterface.
   *
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   *   League\OAuth2\Server\Exception\OAuthServerException.
   */
  protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client) {
    $tiktok_author_code = $this->getRequestParameter('tiktok_author_code', $request);

    if (empty($tiktok_author_code)) {
      throw OAuthServerException::invalidRequest('tiktok_access_token');
    }

    try {
      $tiktok_config = \Drupal::config('social_auth_tiktok_decouple_settings');
      $app_id = $tiktok_config->get('app_id');
      $app_secret = $tiktok_config->get('app_secret');

      /** @var \gimucco\TikTokLoginKit\Connector $connector */
      $connector = new Connector($app_id, $app_secret, '');
      $access_code = $connector->verifyCode($tiktok_author_code);
      $this->socialAuthTiktokManager->setAccessToken($access_code);
      /** @var \League\OAuth2\Client\Provider\AbstractProvider|false $client */
      $client = $this->networkManager->createInstance('social_auth_tiktok_decouple')
        ->getSdk();
      $this->socialAuthTiktokManager->setClient($client);
      /** @var \gimucco\TikTokLoginKit\User $profile */
      $profile = $this->socialAuthTiktokManager->getUserInfo();
      if (!$profile) {
        throw OAuthServerException::invalidCredentials();
      }

      $this->userAuthenticator->setPluginId('social_auth_tiktok_decouple');
      $data = $this->userAuthenticator->checkProviderIsAssociated($profile->getOpenID()) ? NULL : $this->socialAuthTiktokManager->getExtraDetails();
      $this->userAuthenticator->authenticateUser($profile->getDisplayName(),
        $this->socialAuthTiktokManager->getEmailTiktok($profile),
        $profile->getOpenID(),
        $this->socialAuthTiktokManager->getAccessToken(),
        $profile->getAvatar(), $data);
      $this->moduleHandler->invokeAll('simple_oauth_tiktok_connect_after_login', [$profile]);

    }
    catch (\Exception $exception) {
      throw new OAuthServerException(
        $exception->getMessage(),
        $exception->getCode(),
        'invalid_tiktok_credentials',
        400
      );
    }

    if (!$this->currentUser->isAuthenticated()) {
      throw OAuthServerException::invalidCredentials();
    }
    $user = new UserEntity();
    $user->setIdentifier($this->currentUser->id());
    if ($user instanceof UserEntityInterface === FALSE) {
      $this->getEmitter()
        ->emit(new RequestEvent(RequestEvent::USER_AUTHENTICATION_FAILED, $request));

      throw OAuthServerException::invalidCredentials();
    }

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier() {
    return 'tiktok_login_grant';
  }

}
