<?php

namespace Drupal\social_auth_tiktok_decouple\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\social_auth\Form\SocialAuthSettingsForm;

/**
 * Settings form for Social Auth TikTok.
 */
class TiktokAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_auth_tiktok_decouple_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return array_merge(
      parent::getEditableConfigNames(),
      ['social_auth_tiktok_decouple.settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_auth_tiktok_decouple.settings');

    $form['tt_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('TikTok App settings'),
      '#open' => TRUE,
      '#description' => $this->t('You need to first create a TikTok App at <a href="@tiktok-dev">@tiktok-dev</a>', ['@tiktok-dev' => 'https://developers.tiktok.com/']),
    ];

    $form['tt_settings']['app_id'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Application ID'),
      '#default_value' => $config->get('app_id'),
      '#description' => $this->t('Copy the App ID of your TikTok App here. This value can be found from your App Dashboard.'),
    ];

    $form['tt_settings']['app_secret'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('App Secret'),
      '#default_value' => $config->get('app_secret'),
      '#description' => $this->t('Copy the App Secret of your TikTok App here. This value can be found from your App Dashboard.'),
    ];

    $form['tt_settings']['site_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Site URL'),
      '#description' => $this->t('Copy this value to <em>Site URL</em> field of your TikTok App settings.'),
      '#default_value' => $GLOBALS['base_url'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('social_auth_tiktok_decouple.settings')
      ->set('app_id', $values['app_id'])
      ->set('app_secret', $values['app_secret'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
