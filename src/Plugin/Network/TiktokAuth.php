<?php

namespace Drupal\social_auth_tiktok_decouple\Plugin\Network;

use Drupal\Core\Url;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth\Plugin\Network\NetworkBase;
use Drupal\social_auth_tiktok_decouple\Settings\TiktokAuthSettings;
use gimucco\TikTokLoginKit\Connector;

/**
 * Defines a Network Plugin for Social Auth TikTok.
 *
 * @package Drupal\social_auth_tiktok_decouple\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_tiktok_decouple",
 *   social_network = "TikTok",
 *   type = "social_auth",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_tiktok_decouple\Settings\TiktokAuthSettings",
 *       "config_id": "social_auth_tiktok_decouple.settings"
 *     }
 *   }
 * )
 */
class TiktokAuth extends NetworkBase implements TiktokAuthInterface {

  /**
   * Sets the underlying SDK library.
   *
   * @return \gimucco\TikTokLoginKit\Connector|false
   *   The initialized 3rd party library instance.
   *   False if library could not be initialized.
   *
   * @throws \Drupal\social_api\SocialApiException
   *   If the SDK library does not exist.
   */
  protected function initSdk() {

    $class_name = '\gimucco\TikTokLoginKit\Connector';
    if (!class_exists($class_name)) {
      throw new SocialApiException(sprintf('The TikTok library for PHP gimucco TikTokLoginKit not found. Class: %s.', $class_name));
    }

    /** @var \Drupal\social_auth_tiktok_decouple\Settings\TiktokAuthSettings $settings */
    $settings = $this->settings;

    if ($this->validateConfig($settings)) {
      return new Connector($settings->getClientId(), $settings->getClientSecret(), Url::fromRoute('social_auth_tiktok_decouple.callback')->setAbsolute()->toString());
    }

    return FALSE;
  }

  /**
   * Checks that module is configured.
   *
   * @param \Drupal\social_auth_tiktok_decouple\Settings\TiktokAuthSettings $settings
   *   The TikTok auth settings.
   *
   * @return bool
   *   True if module is configured.
   *   False otherwise.
   */
  protected function validateConfig(TiktokAuthSettings $settings) {
    $client_id = $settings->getClientId();
    $client_secret = $settings->getClientSecret();
    if (!$client_id || !$client_secret) {
      $this->loggerFactory
        ->get('social_auth_tiktok_decouple')
        ->error('Define Client ID and Client Secret on module settings.');

      return FALSE;
    }

    return TRUE;
  }

}
