<?php

namespace Drupal\social_auth_tiktok_decouple\Plugin\Network;

use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Defines the TikTok Auth interface.
 */
interface TiktokAuthInterface extends NetworkInterface {}
